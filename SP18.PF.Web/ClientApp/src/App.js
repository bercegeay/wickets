import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Payment } from './components/Payment';
import { Home } from './components/Home';

//Tour Components
import { Tours } from './components/Tour/Tours';
import TourEvents from './components/Tour/TourEvents';

//Member Service Components
import { ProfileUpdate } from './components/MemberServices/ProfileUpdate';
import { MyAccount } from './components/MemberServices/MyAccount';
import { Tickets } from './components/MemberServices/Tickets';
//Event Components
import { Events } from './components/Events/Events';
import SingleEvent from './components/Events/SingleEvent';
import PostProcessing from './components/Events/PostProcessing';


//Venue Components
import { Venues } from './components/Venues/Venues';
import SingleVenue from './components/Venues/SingleVenue';

export default class App extends Component {
    displayName = App.name
    constructor() {
        super();
        this.state = {
        }
    }

  
  render() {
    return (
      <div className="App">
       <Layout>
        <Route exact path='/' component={Home} />
        <Route exact path='/events' component={Events} />
        <Route exact path='/events/postprocessing/:id' component={PostProcessing} />
        <Route exact path='/events/:id' component={SingleEvent} /> 

        <Route exact path='/tours' component={Tours} />
        <Route exact path='/tours/:id' component={TourEvents} />

        <Route  exact path='/venues' component={Venues}/>
        <Route exact path='/venues/:id' component={SingleVenue}/>
                
        <Route path='/payment' component={Payment} />
        <Route path='/ProfileUpdate' component={ProfileUpdate} />
        <Route path='/MyAccount' component={MyAccount} />
        <Route path='/Tickets' component={Tickets} />

       </Layout>



      
      </div>
     
    ); 
  }
}
