﻿import React, { Component } from 'react';
import { Switch, Route } from 'react-router';
import { Link } from 'react-router-dom'; //This is a thing. leave it

import { Media, Badge, Well, Image, ProgressBar, Button, Label, Panel } from 'react-bootstrap';
import SingleEvent from './SingleEvent';
import './Event.css';

export class Events extends Component {
    displayName = Events.name

    constructor(props) {
        super(props);
        this.state = { events: [], event: [], loading: true, search: '' };

        fetch('api/Events')
            .then(response => response.json())
            .then(data => {
                this.setState({ events: data, loading: false });
            });
    }

 static EventTable(events) {


        var str;
        var eventjs;
        var eventdate;
        var jsdate;
        var listlength = events.length;
        var dates = [listlength];
        var truncate;
        var ampm;
        var i;
        for (i in events) {
            str = JSON.stringify(events[i]);
            eventjs = JSON.parse(str);
            eventdate = Date.parse(eventjs.eventStart);
            jsdate = new Date(eventdate);
            jsdate.setSeconds(0);
            truncate = jsdate.toLocaleTimeString();
            ampm = truncate.split(" ");
            truncate = truncate.split(":");
            ampm = ampm.slice(1);
            truncate = truncate.slice(0, 2).join(':');
            dates[i] = (jsdate.toDateString() + ", " + truncate + " " + ampm);
            events[i].eventStart = dates[i];
        };   
     
        return (
            <div>
                <div className="Table">
                    <div className= "col-md-11">
                        {events.map(event =>
                            <div className="row" key={event.id}>

                                <div className="textc">
                                    <Media>
                                        <Well className={event.tourName}>
                                            <Media.Body>

                                                <Media.Heading className="list">
                                                    <div className="col-md-4  headcontainer">
                                                        <Panel className="head">
                                                            <div className="col-md-5 col-xs-3 divmargin">
                                                                {event.tourName}
                                                            </div>
                                                            <div className="col-md-7 col-xs-5 divmargin">
                                                                @ {event.venueName}
                                                            </div>
                                                        </Panel>
                                                    </div>

                                                    <div className="col-md-offset-7 col-md-1">
                                                        <Badge className="badgecolor"> $ {event.ticketPrice} </Badge>
                                                    </div>
                                                </Media.Heading>

                                                <div className="row">
                                                    <div className="datecolor">
                                                        <div className="col-md-offset-8 col-md-4">
                                                            <h4>
                                                                <Label bsStyle="primary">
                                                                {event.eventStart}
                                                                </Label>
                                                            </h4>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div className="row">
                                                    <div>
                                                        <div className="col-md-2">
                                                            <Link to={"/events/" + event.id} > <Button bsStyle="danger"> Buy Ticket </Button></Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Media.Body>
                                        </Well>
                                    </Media>
                                    <hr className="style-two" />
                                </div>;
                            </div>
                        )}
                    </div>
                </div>
            </div>



        );


    }

    render() {

        let content = this.state.loading
            ? <div><ProgressBar active now={45} /></div>
            : Events.EventTable(this.state.events);

        return (

            <div>

                <hr />
                {content}
            </div>
        );
    }



}
