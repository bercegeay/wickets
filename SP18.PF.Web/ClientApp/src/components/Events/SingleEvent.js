import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { Switch, Route } from 'react-router';
import { BrowserRouter as Router, Link } from 'react-router-dom'; //This is a thing. leave it
import './Event.css';
import axios from 'axios';
import creditcardutils from 'creditcardutils';
import { Media, Badge, Well, Image, ProgressBar } from 'react-bootstrap';
import PostProcessing from './PostProcessing';
import { Redirect } from 'react-router';

export default class SingleEvent extends Component {
    displayName = SingleEvent.name

    constructor(props) {
        super(props);
        this.state = {
            event: [],
            id: this.props.match.params.id,
            loading: true,
            card: '',
            cvc: '',
            expMonth: '',
            expYear: '',
        };
        const { card, cvc, expMonth, expYear } = this.state;
        console.log(this.props)

        var creditcardutils = require('creditcardutils');
        let url = SingleEvent.buildUrl(this.state.id);

        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.setState({ event: data, start: '', loading: false });
            });
    }

    static buildUrl(id) {
        let base = "api/Events/"
        base = base + id
        return base
    }

    buyNow(e, response) {

        var inpCard = document.getElementById("card-number").value;
        var inpCvc = document.getElementById("cvc").value;
        var inpExpMonth = document.getElementById("exp-month").value;
        var inpExpYear = document.getElementById("exp-year").value;
        var cardVal = creditcardutils.parseCardType(creditcardutils.formatCardNumber(inpCard));

        var disable = false;

        if (creditcardutils.validateCardNumber(creditcardutils.formatCardNumber(inpCard)) != true) {
            document.getElementById("checkCard").innerHTML = "Please Provide A 16 Digit Card Number";
            disable = true;
        }
        else if (creditcardutils.parseCardType(creditcardutils.formatCardNumber(inpCard)) === 'amex') {
            document.getElementById("checkCard").innerHTML = "Sorry, We Only Accept Visa, MasterCard, and Discover";
            disable = true;
        }
        else {
            document.getElementById("checkCard").innerHTML = "";
        }

        if (creditcardutils.validateCardExpiry(inpExpMonth, inpExpYear) != true) {
            document.getElementById("checkExp").innerHTML = "Please Provide A Valid Expiration Date";
            disable = true;
        }
        else {
            document.getElementById("checkExp").innerHTML = "";
        }
        if (inpCvc.length != 3) {
            document.getElementById("checkCvc").innerHTML = "Invalid CVC";
            disable = true;
        }
        else {
            document.getElementById("checkCvc").innerHTML = "";
        }

        e.preventDefault();

        if (disable === false) {
            this.setState({ redirect: true });
        }
    }
    
    render() {
        //var truncate;
        //var ampm
        //var eventdate = Date.parse(this.state.event.eventStart);
        //var jsdate = new Date(eventdate);
        //jsdate.setSeconds(0);
        //truncate = jsdate.toLocaleTimeString();
        //ampm = truncate.split(" ");
        //truncate = truncate.split(":");
        //ampm = ampm.slice(1);
        //truncate = truncate.slice(0, 2).join(':');
        //var nicedate = (jsdate.toDateString() + ", " + truncate + " " + ampm);
        //this.setState({ start: nicedate })

        const routeEvent = () => (// leave this route It's right
            <Switch>
                <Route path='/events/postprocessing/:id' component={PostProcessing} data={this.state.event} />
            </Switch>
        )

        if (this.state.redirect) {
            return <Redirect push to={"/events/postprocessing/" + this.state.id} />;
        }

        return (
            <div className="SingleEvent">

                
                <div className="textcolor">
                <h2><strong>Event: </strong> {this.state.event.tourName}</h2>
                <h4><strong>Ticket Price: </strong> ${this.state.event.ticketPrice}</h4>

                <div className="row">
                    <div className="col-sm-1">
                        Start Time:
                    </div>
                    <div>
                            {this.state.event.eventStart}
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm-1">
                            End Time:
                    </div>
                    <div>
                        {this.state.event.eventEnd}
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm-1">
                        Venue:
                    </div>
                    <div>
                        {this.state.event.venueName}
                    </div>
                </div>
    
                    <div>
                        <div className="Table">
                            <div className ="row" >
                                <div className ="col-md-4">
                                    <hr />
                                </div>
                            </div>
                            <form className="form-horizontal" role="form">
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label className="control-label" htmlFor="card-holders-name">Name on Card</label>
                                            <input type="text" className="form-control" name="card-holders-name" placeholder="Card Holder's Name" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label className="control-label" htmlFor="card-number">Card Number</label>
                                            <input type="text" className="form-control" name="card-number" id="card-number" placeholder="Debit/Credit Card Number" />
                                        </div>
                                        <div id="checkCard" className="validate"> </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <label>Expiration Date</label>
                                </div>

                                <div className="row">
                                    <div className="form-group">
                                        <div className="col-md-10">
                                            <div className="row">
                                                <div className="col-md-3">
                                                    <select className="form-control" name="exp-month" id="exp-month">
                                                        <option>Month</option>
                                                        <option value="01">Jan (01)</option>
                                                        <option value="02">Feb (02)</option>
                                                        <option value="03">Mar (03)</option>
                                                        <option value="04">Apr (04)</option>
                                                        <option value="05">May (05)</option>
                                                        <option value="06">June (06)</option>
                                                        <option value="07">July (07)</option>
                                                        <option value="08">Aug (08)</option>
                                                        <option value="09">Sep (09)</option>
                                                        <option value="10">Oct (10)</option>
                                                        <option value="11">Nov (11)</option>
                                                        <option value="12">Dec (12)</option>
                                                    </select>
                                                </div>
                                                <div className="col-md-2">
                                                    <select className="form-control" name="exp-year" id="exp-year">
                                                        <option>Year</option>
                                                        <option value="18">2018</option>
                                                        <option value="19">2019</option>
                                                        <option value="20">2020</option>
                                                        <option value="21">2021</option>
                                                        <option value="22">2022</option>
                                                        <option value="23">2023</option>
                                                        <option value="24">2024</option>
                                                        <option value="25">2025</option>
                                                        <option value="26">2026</option>
                                                        <option value="27">2027</option>
                                                        <option value="28">2028</option>
                                                        <option value="29">2029</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="checkExp" className="validate"> </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-offset-3 col-md-1">
                                        <div className="form-group">
                                            <label className="control-label" htmlFor="cvc">Card CVV</label>
                                            <input className="form-control" type="text" name="cvc" id="cvc" placeholder="Code" />
                                        </div>
                                    </div>
                                </div>
                                <div id="checkCvc" className="validate"> </div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="form-group">
                                                <Button onClick={(e) => this.buyNow(e, this.state.response)} bsStyle="success" block> Buy Now </Button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
           </div>
        );
    }
}

