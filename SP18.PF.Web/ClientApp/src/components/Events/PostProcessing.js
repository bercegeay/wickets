import React, { Component } from 'react';
import { Media, Badge, Well, Image, ProgressBar, Button } from 'react-bootstrap';


export default class PostProcessing extends Component {
    displayName = PostProcessing.name

    constructor(props) {
        super(props);

        this.state = {
            ticket: [], response: [], id: this.props.match.params.id, loading: true
        };

        let url = PostProcessing.buildUrl(this.state.id);
        //post to api here

        fetch(url, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                eventId: this.state.id,
            })
        }).then(response => {
            if (response.status >= 400) {
                // console.log(response);
                //  throw new Error('Throw Error');
            }

            this.setState({ response: response, loading: false });
            console.log('State: ', this.state.response);

            //   return (response.json());
        })

    }

    static buildUrl(id) {
        let base = "api/Tickets/Purchase/"
        base = base + id;
        return base
    }

    renderSuccessMessage(ticket, response) {

        if (response.status !== 200) {

            return (<div className="SingleEvent">
                <h2><strong>Status: </strong> {response.statusText} </h2>
                <h4><strong>Something went wrong, please try again. </strong> </h4>

            </div>)
        }

        return (
            <div className="SingleEvent">
                <h2><strong>Status: </strong> {response.statusText} </h2>
                <h4><strong>You're all set, see you at the show. </strong> </h4>

            </div>)

    }


    render() {

        let content = this.state.loading
            ? <div><ProgressBar active now={45} /></div>
            : this.renderSuccessMessage(this.state.ticket, this.state.response);

        return (

            <div className ="textcolor">

                <hr />
                {content}
            </div>
        );





    }
}

