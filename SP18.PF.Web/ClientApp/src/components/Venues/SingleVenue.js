import React, { Component } from 'react';
import {Events} from '../Events/Events';

import { ProgressBar } from 'react-bootstrap';

export default class SingleVenue extends Component {
    displayName = SingleVenue.name

    constructor(props) {
        super(props);

        this.state = {
            events: [],
            venue: [],
            venueId: this.props.match.params.id,
            loading: true
        };

        

        fetch('api/Events')
            .then(response => response.json())
            .then(data => {
                this.setState({ events: data });
            });

        fetch('api/Venues/' + this.state.venueId)
            .then(response => response.json())
            .then(data => {
                this.setState({ venue: data, loading: false });
            });

    }

    render() {

        let filtered = this.state.events.filter((e) => e.venueId == this.state.venueId);
        let today = new Date();
        let filtered2 = filtered.filter((e) => Date.parse(e.eventStart) > today);
        let sorted = filtered2.sort(function(a, b){
            a = Date.parse(a.eventStart);
            b = Date.parse(b.eventStart);

            return (a - b);
        });

        let content = this.state.loading
            ? <div><ProgressBar active now={45} /></div>
            : Events.EventTable(sorted);

        return (

            <div>
                <div className="textgold">
                    <div className=" col-md-11">
                        <h1 align="center"> <strong> Catch A Show at {this.state.venue.name} </strong>  </h1>
                        <hr className="style-seven" />
                    </div>
                    {content}
                    </div>
            </div>
        );
    }
}

