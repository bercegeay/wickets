﻿import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import SingleVenue from './SingleVenue';
import { Media, Badge, Well, Image, ProgressBar, Button, Label, Glyphicon } from 'react-bootstrap';
import './Venues.css';


export class Venues extends Component {
    displayName = Venues.name

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            venues: [],
            venueId: null,
            search: '',
            searchDescription: '',
            searchState: ''
        };

        fetch('api/Venues')
            .then(response => response.json())
            .then(data => {
                this.setState({ venues: data, loading: false, search: '' });
            });

    }
    
    updateSearch(event) {
        this.setState({ search: event.target.value })
    }

    updateSearchDescription(event) {
        this.setState({ searchDescription: event.target.value })
    }

    updateSearchState(event) {
        this.setState({ searchState: event.target.value })
    }

    render() {

        if (this.state.loading) {
            return (<div><ProgressBar active now={45} /></div>)
        }
        

        let filteredVenues = this.state.venues.filter(
            (venue) => {
                    return venue.name.toLowerCase().indexOf(this.state.
                        search.toLowerCase()) !== -1;
                }
        );

        let filteredVenues2 = filteredVenues.filter(
                (venue) => {
                    return venue.description.toLowerCase().indexOf(this.state.
                        searchDescription.toLowerCase()) !== -1;
                }
        );

        let filteredVenues3 = filteredVenues2.filter(
                (venue) => {
                    return venue.physicalAddress.state.toLowerCase().indexOf(this.state.
                        searchState.toLowerCase()) !== -1;
                }
        );
        return (
            <div>
                <div className="table">
                    <div className="col-md-12">
                        <br />
                        <div className="row test">

                            <div className="col-md-4">
                                <input className="form-control"
                                    type="text"
                                    value={this.state.search}
                                    onChange={this.updateSearch.bind(this)}
                                    placeholder="Search Venue Name..." />
                            </div>
                            <div className="col-md-4">
                                <input className="form-control"
                                    type="text"
                                    value={this.state.searchDescription}
                                    onChange={this.updateSearchDescription.bind(this)}
                                    placeholder="Search Venue Description..." />
                            </div>
                            <div className="col-md-4">

                                <select className="form-control" value={this.state.searchState}
                                    onChange={this.updateSearchState.bind(this)}
                                    placeholder="Search Venue State..." >
                                    <option>Search By State</option>
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="DC">District Of Columbia</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>
                                </select>
                            </div>
                        </div>
                        <hr className="shorter"/>
                

                
                    {filteredVenues3.map(venue =>
                        <div className="row" key={venue.id} >
                            <div className="textc" >
                                <Media className="medial">
                                    <Media.Left>
                                        <Image width={128} height={128} src={venue.image} alt="thumbnail" rounded />
                                        <div className="col-sm-offset-2" >
                                            Capacity: <Label bsStyle="info">{venue.capacity}</Label>
                                        </div >
                                    </Media.Left >
                                    <Media.Body>
                                        <Media.Heading>{venue.name}</Media.Heading >
                                        <div className="row" >
                                            <div className="col-md-offset-8 col-md-3" >
                                                <Link to={"/Venues/" + venue.id} > <Button bsStyle="danger"><Glyphicon glyph='th-list' /> Upcoming Events </Button></Link>
                                            </div >
                                        </div >
                                        <div className="row" >
                                            <div className="col-md-4" >
                                                {venue.description}
                                            </div >
                                        </div >

                                        <div className="row" >
                                            <div>
                                                <div className="col-md-4" >

                                                </div >
                                            </div >
                                            <div className="col-md-offset-8 col-md-3" >
                                                <Well bsSize="small" > {venue.physicalAddress.addressLine1} {venue.physicalAddress.zipCode}, {venue.physicalAddress.city}, {venue.physicalAddress.state}</Well >
                                            </div >
                                        </div >
                                    </Media.Body >
                                </Media >
                                <hr className="style-two" />
                            </div >;
                        </div >)}
                    </div>
                </div>
            </div>

);





















            
    }

}