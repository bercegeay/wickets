import React, { Component } from 'react';
import creditcardutils from 'creditcardutils';
import { Button } from 'react-bootstrap';
import { Switch, Route, Link } from 'react-router-dom';

import PostProcessing from './Events/PostProcessing';


function validate(card, cvc, expMonth, expYear) {
    return {
        card: card.length === 0,
        cvc: cvc.length === 0,
        expMonth: expMonth.length === 0,
        expYear: expYear.length === 0
    };
}

export class Payment extends Component {
    displayName = Payment.name
    constructor(props) {
        super(props);
        this.state = {
            card: '',
            cvc: '',
            expMonth: '',
            expYear: '',
            touched: {
                card: false,
                cvc: false,
                expMonth: false,
                expYear: false,
            }
        };
        const { card, cvc, expMonth, expYear } = this.state;

        const routeEvent = () => (// leave this route It's right
            <Switch>
                <Route path='/events/postprocessing/:id' component={PostProcessing} data={this.state.event} />
            </Switch>
        )

        var creditcardutils = require('creditcardutils');
    }

    cardValue(e) {
        this.setState({ card: e.target.value });
    }
    cvcValue(e) {
        this.setState({ cvc: e.target.value });
    }
    expMonthValue(e) {
        this.setState({ expMonth: e.target.value });
    }
    expYearValue(e) {
        this.setState({ expYear: e.target.value });
    }

    handleSubmit = (e) => {
        if (!this.canBeSubmitted()) {
            e.preventDefault();
            return;
        }
    }

    canBeSubmitted() {
        const errors = validate(this.state.card, this.state.cvc, this.state.expMonth, this.state.expYear);
            const isDisabled = Object.keys(errors).some(x => errors[x]);
            return !isDisabled;
        }

        handleBlur = (field) => (e) => {
            this.setState({
                touched: { ...this.state.touched, [field]: true },
            });
        }

        render() {

            const errors = validate(this.state.card, this.state.cvc, this.state.expMonth, this.state.expYear);
            const isEnabled =
                !Object.keys(errors).some(x => errors[x]) &&
                creditcardutils.validateCardNumber(creditcardutils.formatCardNumber(this.state.card)) === true &&
                creditcardutils.parseCardType(creditcardutils.formatCardNumber(this.state.card)) !== "amex" &&
                creditcardutils.validateCardExpiry(this.state.expMonth, this.state.expYear) === true &&
                this.state.cvc.length === 3 &&
                this.state.expMonth.length === 2 &&
                this.state.expYear.length === 2;

            const shouldMarkError = (field) => {
                const hasError = errors[field];

                const shouldShow = this.state.touched[field];

                return hasError ? shouldShow : false;
            }

            return (
                <div>
                    <h1>Pay Here</h1>
                    <form>
                        <input onChange={this.cardValue.bind(this)} onBlur={this.handleBlur('card')} type="text" placeholder="Enter Card Number" name="card" ref="card" />
                        <br />
                        <br />
                        <input onChange={this.cvcValue.bind(this)} onBlur={this.handleBlur('cvc')} type="text" placeholder="Enter CVC" name="cvc" ref="cvc" />
                        <br />
                        <br />
                        <input onChange={this.expMonthValue.bind(this)} onBlur={this.handleBlur('expMonth')} type="text" placeholder="Expiration Month" name="expMonth" ref="expMonth" />
                        <br />
                        <br />
                        <input onChange={this.expYearValue.bind(this)} onBlur={this.handleBlur('expYear')} type="text" placeholder="Expiration Year" name="expYear" ref="expYear" />
                        <br />
                        <br />
                        <Link to={"/events/postprocessing/"} > <Button disabled={!isEnabled} bsStyle="success" block> Buy Now </Button> </Link>
                        <br />
                        <br />
                    </form>
                </div>
            );
        }
    }