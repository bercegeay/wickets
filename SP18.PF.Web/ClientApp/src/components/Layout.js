import React, { Component } from 'react';
import { Col, Grid, Row } from 'react-bootstrap';
import { NavMenu } from './Navigation/NavMenu';
import { MemberNav } from './Navigation/MemberNav';

export class Layout extends Component {
    displayName = Layout.name

    constructor(props) {
        super(props);
        this.state = { user: [] };

        fetch('api/users/getme',
            { credentials: 'include' })
            .then(response => response.json())
            .then(data => {
                this.setState({ user: data });
            });
    }
    
    isMember(role) {

        if (role === "Admin" || role === "Customer") {
            return (<MemberNav />);
        }

        return (<NavMenu />);
    }


    render() {

    var user = this.state.user;
    var role = "nonmember";
    {user.map(user => role=user.role)}

    let content = this.isMember(role);

    return (
        <Grid fluid>
            <Row>
                <Col sm={3}>
                    {content}
                </Col>
                <Col sm={9}>
                    {this.props.children}
                </Col>
            </Row>
        </Grid>
    );
  }
}
