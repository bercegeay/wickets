import React, { Component } from 'react';
import { Col, Grid, Row, Table, Carousel, Jumbotron, Image, ProgressBar, Panel } from 'react-bootstrap';
import {Events} from './Events/Events';


export class Home extends Component {
    displayName = Home.name

    render() {

      return (
          <Grid fluid>
              <Row>
                  <Col md={12}>
                          <Image className="headd" src="https://www.experiencegroup.co.nz/imageLib//Upcoming%20events.jpg" responsive rounded />
                      <br />
                      <Panel className="welcomepanel">
                          <div className="homemessage">
                              WELCOME TO WICKETS!
                          </div>
                        </Panel>
                          <br/>
                  </Col>
              </Row>
              <Row>
                  <Col md={5}>
                      <Table>
                          <tr>
                              <td>
                                  <Carousel bsStyle="carousel">
                                      <Carousel.Item align="center">
                                          <Image src="https://2ab9pu2w8o9xpg6w26xnz04d-wpengine.netdna-ssl.com/wp-content/uploads/2018/01/26904442_10156476524727437_1926677820166810202_n.jpg" responsive />
                                      </Carousel.Item>
                                      <Carousel.Item align="center">
                                          <Image src="https://pbs.twimg.com/media/DYF9psQVMAEzGka?format=jpg" alt="promo" responsive />
                                      </Carousel.Item>
                                      <Carousel.Item align="center" className="flyer">
                                          <img height={300} width={475} src="http://www.theuntz.com/media/events/4/1/9/41969/backwoods-at-mulberry-mountain-2018_large.jpg" />
                                      </Carousel.Item>
                                  </Carousel>
                              </td>
                          </tr>
                      </Table>
                  </Col>
                  <Col md={5}>
                          <div>
                          <Carousel
                              interval={7500}
                              indicators={false}
                              nextIcon={null}
                              prevIcon={null}
                              className="sponsored1">
                                  <Carousel.Item animateOut={true} animateIn={true} className="sponsored">
                                      <Image src="https://i.pinimg.com/originals/2e/00/e7/2e00e7db7a244359c7e267547119acde.jpg" responsive />
                                  </Carousel.Item>
                                  <Carousel.Item className="sponsored" >
                                    <Image src="https://i.ticketweb.com//i/venue/338005_Venue.jpg?v=7"  responsive/>
                                  </Carousel.Item>
                              </Carousel>
                      </div>
                      <div>
                          <Carousel
                              interval={12500}
                              indicators={false}
                              nextIcon={null}
                              prevIcon={null}
                              className="sponsored2">
                              <Carousel.Item className="sponsored2" >
                                  <Image src="https://i2.wp.com/52showsayear.com/wp-content/uploads/2018/03/papadosio-aqueous.jpg?fit=720%2C405" responsive />
                              </Carousel.Item>
                              <Carousel.Item className="sponsored2" >
                                  <Image src="http://www.dirtyheads.com/wp-content/uploads/2017/06/dirtyheads_og.jpg" responsive />
                              </Carousel.Item>

                            </Carousel>
                        </div>
                      
                  </Col>
              </Row>
          </Grid>
      );
  }
}
