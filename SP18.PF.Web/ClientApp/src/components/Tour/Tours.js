﻿import React, { Component } from 'react';
import { Media, Image, ProgressBar, Button, Glyphicon } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Tour.css';

export class Tours extends Component {
    displayName = Tours.name

    constructor(props) {
        super(props);
        
        this.state = {
            tours: [],
            loading: true,
            tourId: null,
            search: ''
        };

        fetch('/api/tours')
            .then(response => response.json())
            .then(data => {
                this.setState({ Tours: data, loading: false });
                console.log(data);
            });
    }


    updateSearch(event) {
        this.setState({ search: event.target.value })
    }


    render() {
        if (this.state.loading) {
            return (<div><ProgressBar active now={45} /></div>)
        }
        let filteredTours = this.state.Tours.filter(
            (tour) => {
                return tour.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
            }
        );

        return (
            <div>
                <div className="table">
                    <div className= "col-md-12">
                        <br/>
                        <div className="row">
                            <input className="form-control"
                                type="text"
                                value={this.state.search}
                                onChange={this.updateSearch.bind(this)}
                                placeholder="Search Tours..." />
                        </div>
                        <br/>
                        {filteredTours.map(tour =>
                            <div className="row" key={tour.id}>
                                <div className ="textc">
                                    <Media>
                                        <Media.Left>
                                            <Image width={128} height={128} src={tour.image} alt="thumbnail" rounded />
                                        </Media.Left>
                                        <Media.Body>
                                            <Media.Heading>{tour.name}</Media.Heading>
                                            <div className="row">
                                                <div>
                                                    <div className="col-md-6">
                                                        <b>{tour.description}</b>
                                                    </div>
                                                </div>
                                                <div className="col-md-offset-8 col-md-3">
                                                    <Link to={"/Tours/" + tour.id} > <Button bsStyle="danger"><Glyphicon glyph='th-list' /> Upcoming Events </Button></Link>
                                                </div>
                                            </div>
                                        </Media.Body>
                                    </Media>
                                    <hr className="style-two" />
                                </div>
                            </div>
                        )}

                    </div>
                </div>
            </div>
        );
    }
}