﻿import React, { Component } from 'react';
import { Events } from '../Events/Events';

import { ProgressBar } from 'react-bootstrap';

export default class TourEvents extends Component {
    displayName = TourEvents.name

    constructor(props) {
        super(props);

        this.state = {
            events: [],
            tour: [],
            Id: this.props.match.params.id,
            loading: true
        };
        console.log(this.props)



        fetch('api/Events')
            .then(response => response.json())
            .then(data => {
                this.setState({ events: data, loading: false });
            });

        fetch('api/tours/' + this.state.Id)
            .then(response => response.json())
            .then(data => {
                this.setState({ tour: data, loading: false });
            });
    }


    render() {

        let filtered = this.state.events.filter((e) => e.tourId == this.state.Id);
        let today = new Date();
        let filtered2 = filtered.filter((e) => Date.parse(e.eventStart) > today);

        let sorted = filtered2.sort(function (a, b) {
            a = Date.parse(a.eventStart);
            b = Date.parse(b.eventStart);

            return (a - b);
        });

        let content = this.state.loading
            ? <div><ProgressBar active now={45} /></div>
            : Events.EventTable(sorted);

        return (

            <div className="textgold">
                <div className="col-md-11">
                    <h1 align="center"> <strong> Looking For {this.state.tour.name}?</strong></h1>
                    <hr className="style-seven" />
                </div>
                {content}
            </div>
        );
    }
}