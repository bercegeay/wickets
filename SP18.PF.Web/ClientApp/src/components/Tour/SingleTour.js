import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import {Events} from '../Events/Events';

import { ProgressBar } from 'react-bootstrap';

export default class SingleTour extends Component {
    displayName = SingleTour.name

    constructor(props) {
        super(props);

        this.state = {
            events: [], tour: [], tourId: this.props.match.params.id, loading: true
        };
        console.log(this.props)

        

        fetch('api/Events')
            .then(response => response.json())
            .then(data => {
                this.setState({ events: data });
            });

        fetch('api/Tours/' + this.state.tourId)
            .then(response => response.json())
            .then(data => {
                this.setState({ tour: data, loading: false });
            });

    }


    render() {

        let content = this.state.loading
            ? <div><ProgressBar active now={45} /></div>
            : Events.EventTable(this.state.events.filter((e) => e.tourId == this.state.tourId));

        return (

            <div>
                <h1> <strong> Catch {this.state.tour.name} on Tour! </strong>  </h1>

                <hr />
                {content}
            </div>
        );
    }
}

