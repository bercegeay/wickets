﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem, Button, ButtonToolbar, NavDropdown, MenuItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { LoginModal } from './LoginModal';
import { RegisterModal } from './RegisterModal';
import './NavMenu.css';

export class NavMenu extends Component {
  displayName = NavMenu.name



  render() {
    return (
      <Navbar inverse fixedTop fluid collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={'/'}><b>Wickets</b></Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to={'/'} exact>
              <NavItem>
                <Glyphicon glyph='home' /> Home
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/Venues'}>
              <NavItem>
                <Glyphicon glyph='modal-window' /> Venues
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/Tours'}>
                <NavItem>
                    <Glyphicon glyph='cd' /> Tours
              </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>

        <Navbar.Collapse className="bottom">
                <Nav>
                    
                    <NavDropdown
                        dropup
                        pullRight
                        eventKey={3}
                        title="Members"
                        id="navdrop">
                        <MenuItem eventKey={3.1}>
                                <LoginModal />
                            </MenuItem>

                            <MenuItem divider />

                            <MenuItem eventKey={3.2}>
                                <RegisterModal />
                            </MenuItem>
                    </NavDropdown>
                </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
