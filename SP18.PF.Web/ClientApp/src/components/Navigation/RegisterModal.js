﻿import React, { Component } from 'react';
import { Modal, Button, Glyphicon } from 'react-bootstrap';
import axios from 'axios';
import './Login.css';




export class RegisterModal extends Component {

    displayName = RegisterModal.name

    constructor(props, context) {
        super(props, context);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {
            show: false,
            email: '',
            password: '',
            confirmPassword: '',
            addressLine1: '',
            addressLine2: '',
            zipCode: '',
            city: '',
            state: ''
        };
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    emailValue(e) {
        this.setState({ email: e.target.value });
    }
    passValue(e) {
        this.setState({ password: e.target.value });
    }
    confirmpassValue(e) {
        this.setState({ confirmPassword: e.target.value });
    }
    address1Value(e) {
        this.setState({ addressLine1: e.target.value });
    }
    address2Value(e) {
        this.setState({ addressLine2: e.target.value });
    }
    zipValue(e) {
        this.setState({ zipCode: e.target.value });
    }
    cityValue(e) {
        this.setState({ city: e.target.value });
    }
    stateValue(e) {
        this.setState({ state: e.target.value });
    }

    Register(e) {
        e.preventDefault();
        axios.post('/api/users/register', {
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword,
            billingAddress: {
                addressLine1: this.state.addressLine1,
                addressLine2: this.state.addressLine2,
                zipCode: this.state.zipCode,
                city: this.state.city,
                state: this.state.state
            }
        })
            .then(function (response) {
                if (response.status == 200) {
                    window.location.reload();
                }
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    render() {

        return (

            <div>
                <Button bsStyle="primary" onClick={this.handleShow} block>
                    <Glyphicon glyph='copy' />
                    Register
                </Button>

                <Modal show={this.state.show} onHide={this.handleClose} bsSize="small">

                    <Modal.Header closeButton>
                        <Modal.Title>Registration</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10">
                                <form>
                                    <div class="row">
                                        <div>
                                            <input onChange={this.emailValue.bind(this)} type="email" class="form-control" id="email" placeholder="Email Address" ref="email" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div>
                                            <input onChange={this.passValue.bind(this)} type="password" class="form-control" id="pwd" placeholder="Password" ref="password" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div>
                                            <input onChange={this.confirmpassValue.bind(this)} type="password" class="form-control" name="confirmPassword" placeholder="Confirm Password" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div>
                                            <input onChange={this.address1Value.bind(this)} class="form-control" type="text" placeholder="Address Line 1" name="addressLine1" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div>
                                            <input onChange={this.address2Value.bind(this)} class="form-control" type="text" placeholder="Address Line 2" name="addressLine2" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div>
                                            <input onChange={this.zipValue.bind(this)} class="form-control" type="text" placeholder="Zip Code" name="zipCode" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div>
                                            <input onChange={this.cityValue.bind(this)} class="form-control" type="text" placeholder="City" name="city" />
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div>
                                            <input onChange={this.stateValue.bind(this)} class="form-control" type="text" placeholder="State" name="state" />
                                        </div>
                                    </div>


                                    <div class="row">
                                        <br />
                                    </div>

                                    <div class="row">
                                        <div>
                                            <Button id="sum" onClick={(e) => this.Register(e)} block>Register Now</Button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </Modal.Body>

                </Modal>
            </div>
        );
    }
}