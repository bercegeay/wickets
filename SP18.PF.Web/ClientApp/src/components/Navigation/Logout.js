﻿import React, { Component } from 'react';
import { Button, Glyphicon } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

export class Logout extends Component {

    Logout(e) {
        fetch('api/users/logout', { credentials: 'include' })

        window.location.reload();
    }

    render() {

        return (

            <div className="row">
                <div >
                    <Button bsStyle="danger" onClick={(e) => this.Logout(e)} block> <Glyphicon glyph='off' /> Logout </Button> 
                </div>
            </div>
            
            )
    }
}