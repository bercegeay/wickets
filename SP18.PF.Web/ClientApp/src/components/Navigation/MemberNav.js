﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem, Button } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Logout } from './Logout';
import './NavMenu.css';

export class MemberNav extends Component {
    displayName = MemberNav.name

    logout() {
        fetch('/api/users/logout')
    }

    render() {
        return (
            <Navbar inverse fixedTop fluid collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link to={'/'}><b>Wickets</b></Link>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <LinkContainer to={'/'} exact>
                            <NavItem>
                                <Glyphicon glyph='home' /> Home
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/Venues'}>
                            <NavItem>
                                <Glyphicon glyph='modal-window' /> Venues
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to={'/Tours'}>
                            <NavItem>
                                <Glyphicon glyph='cd' /> Tours
                            </NavItem>
                        </LinkContainer>
                    </Nav>
                </Navbar.Collapse>

                <Navbar.Collapse className="bottom">
                    <Nav>
                        <LinkContainer to={'/MyAccount'}>
                            <NavItem>
                                <Glyphicon glyph='user' /> My Account
                            </NavItem>
                        </LinkContainer>
                        < NavItem >
                            <div>
                                <Logout />  
                            </div>
                        </NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}