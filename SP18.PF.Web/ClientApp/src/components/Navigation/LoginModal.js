﻿import React, { Component } from 'react';
import { Modal, Button, Glyphicon } from 'react-bootstrap';
import axios from 'axios';


export class LoginModal extends Component {

    displayName = LoginModal.name

    constructor(props, context) {
        super(props, context);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.showHide = this.showHide.bind(this);

        this.state = {
            show: false,
            email: '',
            password: '',
            rememberMe: true
        };
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    check() {
        var i = 7;
        return (i);
    }


    emailValue(e) {
        this.setState({ email: e.target.value }); // gets the value from the email textbox
    }
    passValue(e) {
        this.setState({ password: e.target.value }); // gets the value from the password textbox
    }

    remember(event) {// sets the boolean if box is checked or not
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;

        this.setState({
            rememberMe: value
        });
    }

   
    SignIn(e) {
       
        var inpEmail = document.getElementById("email").value;
        var inpPass = document.getElementById("pwd").value;

        if (inpEmail === "") {
            document.getElementById("checkEmail").innerHTML = "Please Provide A Valid Email Address";
        }
        else {
            document.getElementById("checkEmail").innerHTML = "";
        }

        if (inpPass === "") {
            document.getElementById("checkPass").innerHTML = "Please Enter Your Password";
        }
        else {
            document.getElementById("checkPass").innerHTML = "";
        }

        e.preventDefault();
        axios.post('/api/users/login', { // this uses axios to pass a POST to the API
            email: this.state.email, //enter in the email from the form
            password: this.state.password, // enter in the password from the form
            rememberMe: this.state.rememberMe // sends true or false depending on checkbox
        })
            .then(response => {
                if (response.status == 200) {
                    window.location.reload();
                }
            })
            .catch(function (error) {
                document.getElementById("checkLogin").innerHTML = "Invalid Username or Password";
                console.log("hello?");
            });
    }

    showHide(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            type: this.state.type === 'input' ? 'pwd' : 'input'
        })
    }

    render() {

        return (

            <div>
                <Button bsStyle="success" onClick={this.handleShow} block>
                    <Glyphicon glyph='user' />
                    Login
                </Button>

                <Modal show={this.state.show} onHide={this.handleClose} bsSize="small">

                    <Modal.Header closeButton>
                        <Modal.Title>Member Services</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">
                            <div className="col-md-offset-1 col-md-10">
                        <form>
                            <div className="row">
                                <div>
                                    <input onChange={this.emailValue.bind(this)}  className="form-control" id="email" placeholder="Email Address" />
                                </div>
                                <div id="checkEmail" className="validate"> </div>
                            </div>
                            <div className="row">
                                <div>
                                    <input onChange={this.passValue.bind(this)} type="password" className="form-control" id="pwd" placeholder="Password"  />
                                </div>
                                <div id="checkPass" className="validate">  </div>
                            </div>
                            
                            <div className="row">
                                <div className="checkbox">
                                    <label><input onChange={this.remember.bind(this)} type="checkbox" ref="rememberMe" /> Remember Me</label>
                                </div>
                            </div>

                            <div className="row">
                                <br />
                            </div>

                            <div className="row">
                                <div id ="checkLogin" className="logval"> </div>
                                        <div>
                                            <Button id="sum" onClick={(e) => this.SignIn(e)} block>Sign-In</Button>
                                </div>
                            </div>
                        </form>
                                </div>
                            </div>
                    </Modal.Body>
                   
                </Modal>
            </div>
        );
    }
}