﻿import React, { Component } from 'react';


export class Purchase extends Component {
    displayName = Purchase.name

    constructor(props) {
        super(props);
        this.state = { ticket: [], user: [], loading: true };

    
        fetch(buildUrl(this.state.ticket.event.id))
          .then(response => response.json())
          .then(data => {
              this.setState({ ticket: data, loading: false });   
          });
    }
    buildUrl(id) {
        let base = "api/tickets/purchase/"
        base = base + id;
        return base
    }
    
       static renderPurchaseForm(event) {
           return (
               <form className="form-horizontal" role="form">
                   <div className="form-group">
                       <label className="control-label" htmlFor="card-holders-name">Name on Card</label>
                       <input type="text" className="form-control" name="card-holders-name" placeholder="Card Holder's Name" />
                   </div>

                   <div className="form-group">
                       <label className="control-label" htmlFor="card-number">Card Number</label>
                       <input type="text" className="form-control" name="card-number" id="card-number" placeholder="Debit/Credit Card Number" />
                   </div>

                   <div className="form-group">
                       <label className="col-sm-3 control-label" for="exp-month">Expiration Date</label>
                       <div className="col-sm-9">
                           <div className="row">
                               <div className="col-xs-3">
                                   <select className="form-control col-sm-2" name="exp-month" id="exp-month">
                                       <option>Month</option>
                                       <option value="01">Jan (01)</option>
                                       <option value="02">Feb (02)</option>
                                       <option value="03">Mar (03)</option>
                                       <option value="04">Apr (04)</option>
                                       <option value="05">May (05)</option>
                                       <option value="06">June (06)</option>
                                       <option value="07">July (07)</option>
                                       <option value="08">Aug (08)</option>
                                       <option value="09">Sep (09)</option>
                                       <option value="10">Oct (10)</option>
                                       <option value="11">Nov (11)</option>
                                       <option value="12">Dec (12)</option>
                                   </select>
                               </div>
                               <div className="col-xs-3">
                                   <select className="form-control" name="exp-year">
                                       <option value="19">2019</option>
                                       <option value="20">2020</option>
                                       <option value="21">2021</option>
                                       <option value="22">2022</option>
                                       <option value="23">2023</option>
                                       <option value="24">2024</option>
                                       <option value="25">2025</option>
                                       <option value="26">2026</option>
                                       <option value="27">2027</option>
                                       <option value="28">2028</option>
                                       <option value="29">2029</option>
                                   </select>
                               </div>
                           </div>
                       </div>
                   </div>

                   <div className="form-group">
                       <label className="col-sm-3 control-label" for="cvv">Card CVV</label>
                       <div className="col-sm-3">
                           <input type="text" className="form-control" name="cvv" id="cvv" placeholder="Security Code" />
                </div>
                       </div>
                       <div className="form-group">
                           <div className="col-sm-offset-3 col-sm-9">
                               <button type="button" className="btn btn-success">Pay Now</button>
                           </div>
                       </div>

               </form>


          //     <form className='form'>
          //  <thead>
          //    <tr>
          //      <th><strong>Tour</strong></th>
          //      <th><strong>Date</strong></th>
          //      <th><strong>Venue</strong></th>
          //      <th><strong>Ticket Price</strong></th>
          //    </tr>
          //  </thead>
          //  <tbody>
          //    {events.map(event =>
          //      <tr key={event.tourName}>
          //        <td>{event.tourName}</td>
          //        <td>{event.eventStart}</td>
          //        <td>{event.venueName}</td>
          //        <td>{event.ticketPrice}</td>
          //      </tr>
          //    )}
          //  </tbody>
          //</form>
        );
      }
    
      render() {
        let contents = this.state.loading
          ? <p><em>Loading...</em></p>
          : Events.renderEventsTable(this.state.events);
    
        return (
          <div>
            <h1>Events</h1>
            {contents}
          </div>
        );
      }
    
}