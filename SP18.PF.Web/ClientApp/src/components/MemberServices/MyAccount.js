﻿import React, { Component } from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import { ProfileUpdate } from './ProfileUpdate';
import { Tickets } from './Tickets';

export class MyAccount extends Component {
    displayName = MyAccount.name
    constructor(props) {
        super(props);
        this.state = { tickets: [], loading: true };
    }


  
    render() {
        return(
        <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
            <Tab bsClass = "tabs" eventKey={1} title="Welcome">
                <h2>Welcome To Your Wickets Account!</h2>
            </Tab>
            <Tab className = "tabs" eventKey={2} title="Profile">
                    <ProfileUpdate />
            </Tab>
            <Tab eventKey={3} title="My Tickets" >
                <Tickets />
            </Tab>
            </Tabs>
        );
    }
}