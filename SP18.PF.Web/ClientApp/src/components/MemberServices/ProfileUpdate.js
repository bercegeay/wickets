﻿import React, { Component } from 'react';
import { Button } from 'react-bootstrap'
import axios from 'axios';
export class ProfileUpdate extends Component {
    displayName = ProfileUpdate.name

    constructor(props) {
        super(props);
        this.state = {
            addressLine1: '',
            addressLine2: '',
            zipCode: '',
            city: '',
            state: ''
        };

    }

    address1Value(e) {
        this.setState({ addressLine1: e.target.value });
                        }
    address2Value(e) {
        this.setState({ addressLine2: e.target.value });
                    }
    zipValue(e) {
        this.setState({ zipCode: e.target.value });
                }
    cityValue(e) {
        this.setState({ city: e.target.value });
                }
    stateValue(e) {
        this.setState({ state: e.target.value });
                }

    ProfileUpdate = (e) => {
        e.preventDefault();
        axios.put('/api/users/billing-info', {
            
                addressLine1: this.state.addressLine1,
                addressLine2: this.state.addressLine2,
                zipCode: this.state.zipCode,
                city: this.state.city,
                state: this.state.state
            
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    render() {
        return (
            <div>
                <h1 align="center">Update Billing Address </h1>
                <form>
                    <input className="form-control" onChange={this.address1Value.bind(this)} type="text" placeholder="Address Line 1" name="addressLine1" />
                    <br />
                    <input className="form-control" onChange={this.address2Value.bind(this)} type="text" placeholder="Address Line 2" name="addressLine2" />
                    <br />
                    <input className="form-control" onChange={this.zipValue.bind(this)} type="text" placeholder="Zip Code" name="zipCode" />
                    <br />
                    <input className="form-control" onChange={this.cityValue.bind(this)} type="text" placeholder="City" name="city" />
                    <br />
                    <input className="form-control" onChange={this.stateValue.bind(this)} type="text" placeholder="State" name="state" />

                    <br />
                    <Button onClick={this.ProfileUpdate}>Update Profile</Button>

                </form>
            </div>
        );
    }
}