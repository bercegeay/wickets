﻿import React, { Component } from 'react';

export class Tickets extends Component {
    displayName = Tickets.name

    constructor(props) {
        super(props);
        this.state = { tickets: [], loading: true };

        //if the fetch uses credentials it needs the credentials in the request
        fetch('api/tickets', { credentials: 'same-origin' })
            .then(response => response.json())
            .then(data => {
                this.setState({ tickets: data, loading: false });
                
            });

    }

     renderTicketsTable(tickets) {


         var str;
         var eventjs;
         var eventdate;
         var jsdate;
         var listlength = tickets.length;
         var dates = [listlength];
         var truncate;
         var ampm;
         var i;
         for (i in tickets) {
             str = JSON.stringify(tickets[i].event);
             eventjs = JSON.parse(str);
             eventdate = Date.parse(eventjs.eventStart);
             jsdate = new Date(eventdate);
             jsdate.setSeconds(0);
             truncate = jsdate.toLocaleTimeString();
             ampm = truncate.split(" ");
             truncate = truncate.split(":");
             ampm = ampm.slice(1);
             truncate = truncate.slice(0, 2).join(':');
             dates[i] = (jsdate.toDateString() + ", " + truncate + " " + ampm);
             tickets[i].event.eventStart = dates[i];
         }; 


         return (
            <div className="textcolor">
            <div className='Table'>
                <div>
                    <div className="row">
                        <div className="col-md-2"><strong>Tour</strong></div>
                        <div className="col-md-3"><strong>Date</strong></div>
                        <div className="col-md-2"><strong>Venue</strong></div>
                        <div className="col-md-2"><strong>Ticket Price</strong></div>
                    </div>
                </div>
                <div>
                    {tickets.map(ticket =>
                        //set the mapping of the ticket attributes here
                        <div className="row" key={ticket.event.id}>
                            <div className="col-md-2">{ticket.event.tourName}</div>
                            <div className="col-md-3">{ticket.event.eventStart}</div>
                            <div className="col-md-2">{ticket.event.venueName}</div>
                            <div className="col-md-2">${ticket.event.ticketPrice}</div>
                        </div>
                    )}
                </div>
            </div>
            </div>
        );
    }

    render() {
        
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderTicketsTable(this.state.tickets);

        return (
            <div>
                <h1>Tickets</h1>
                {contents}
            </div>
        );
    }

}