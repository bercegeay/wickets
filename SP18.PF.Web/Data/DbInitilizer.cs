﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using SP18.PF.Core.Features.Events;
using SP18.PF.Core.Features.Shared;
using SP18.PF.Core.Features.Tickets;
using SP18.PF.Core.Features.Tours;
using SP18.PF.Core.Features.Users;
using SP18.PF.Core.Features.Venues;
using SP18.PF.Web.Helpers;

namespace SP18.PF.Web.Data
{
    public static class DbInitilizer
    {
        public static void SeedData(DbContext dataContext)
        {
            SeedUsers(dataContext);
            SeedVenues(dataContext);
            SeedTours(dataContext);
            SeedScheduledEvents(dataContext);
            SeedTickets(dataContext);
        }

        private static void SeedTickets(DbContext dataContext)
        {
            var tickets = dataContext.Set<Ticket>();
            if (tickets.Any())
            {
                return;
            }
            var events = dataContext.Set<Event>().ToArray();
            var users = dataContext.Set<User>().ToArray();
            for (int i = 0; i < 40; i++)
            {
                var @event = events[i % events.Length];
                var user = users[i % users.Length];

                tickets.Add(new Ticket
                {
                    Event = @event,
                    User = user,
                    PurchasePrice = @event.TicketPrice
                });
            }
            dataContext.SaveChanges();
        }

        private static void SeedScheduledEvents(DbContext dataContext)
        {
            var events = dataContext.Set<Event>();
            if (events.Any())
            {
                return;
            }
            var tours = dataContext.Set<Tour>().Select(x => x.Id).ToArray();
            var venues = dataContext.Set<Venue>().Select(x => x.Id).ToArray();

            for (int i = 0; i < 8; i++)
            {
                var tour = tours[i];
                for (int j = 0; j < 4; j++)
                {
                    var venue = venues[j];
                    var start = DateTimeOffset.Now.AddDays(4 + j + i).AddHours(i + j);
                    events.Add(new Event
                    {
                        TourId = tour,
                        VenueId = venue,
                        TicketPrice = (i + 6) * 3,
                        EventStart = start,
                        EventEnd = start.AddHours(j + 1)
                    });
                }
            }
            dataContext.SaveChanges();
        }

        private static void SeedTours(DbContext dataContext)
        {
            var tours = dataContext.Set<Tour>();
            if (tours.Any())
            {
                return;
            }

            tours.Add(new Tour
            {
                Name = "The Floozies",
                Description = "Funk Jesus",
                Image = "https://logjampresents.com/wp-content/uploads/2017/11/The-Floozies-5.jpg"
            }
                );
            tours.Add(new Tour
            {
                Name = "Tupac",
                Description = "It's Just a Hologram",
                Image = "http://l3.yimg.com/bt/api/res/1.2/1YHBH6CUQ_2o4zbWNzPcRg--/YXBwaWQ9eW5ld3M7cT04NQ--/http://mit.zenfs.com/888/2012/04/tupacshakurhologramcoachella.jpg"
            }
                );
            tours.Add(new Tour
            {
                Name = "Saliva",
                Description = "Only the Sickest Survive",
                Image = "https://randyraisch.files.wordpress.com/2011/03/saliva2.jpg"
            }
                );
            tours.Add(new Tour
            {
                Name = "Citizen Cope",
                Description = "Counting Crows: Somewhere Under Wonderland",
                Image = "https://image-ticketfly.imgix.net/00/01/49/83/05-og.jpg?w=1577&h=2048"
            }
                );
            tours.Add(new Tour
            {
                Name = "Dirty Heads",
                Description = "Summer Tour",
                Image = "https://s3.amazonaws.com/creativeallies/snapshots/000/037/560/original/MATISYAHU_copy.jpg?1349910247"
            }
                );
            tours.Add(new Tour
            {
                Name = "Hopsin",
                Description = "Borderline Tour",
                Image = "http://embracepresents.com/wp-content/uploads/2017/02/hopsin-web.jpg"
            }
                );
            tours.Add(new Tour
            {
                Name = "Papadosio",
                Description = "Serenity",
                Image = "https://www.jambase.com/wp-content/uploads/photo-gallery/20160507%20Papadosio%20Red%20Rocks%20Phierce%20Photography/Papadosio%20Red%20Rocks%20Phierce%20Photo%20(3).jpg"
            }
                );
            tours.Add(new Tour
            {
                Name = "Linkin Park",
                Description = "One More Light World Tour",
                Image = "http://www.be-subjective.de/wp-content/uploads/2017/06/bs_linkin_park_one_more_light.jpg"
            }
                );

            dataContext.SaveChanges();
        }

        private static void SeedVenues(DbContext dataContext)
        {
            var venues = dataContext.Set<Venue>();
            if (venues.Any())
            {
                return;
            }

            venues.Add(
                new Venue
                {
                    Name = "The Republic",
                    PhysicalAddress = new Address
                    {
                        AddressLine1 = "828 S Peters Street",
                        City = "New Orleans",
                        State = "LA",
                        ZipCode = "70130"
                    },
                    Capacity = 200,
                    Description = "Warehouse turned concert venue & club (Thursdays–Saturdays) with wooden beams & crystal chandeliers.",
                    Image = "https://pbs.twimg.com/profile_images/697920569715486720/BsqgIkIP.jpg",
                    SImage = "http://wintercircleproductions.com/wp-content/themes/wcp-responsive/assets/img/venues/republic.jpg"
                }
                );
            venues.Add(
                new Venue
                {
                    Name = "Varsity Theater",
                    PhysicalAddress = new Address
                    {
                        AddressLine1 = "3353 Highland Rd",
                        City = "Baton Rouge",
                        State = "LA",
                        ZipCode = "70802"
                    },
                    Capacity = 80,
                    Description = "Live music from touring acts & other events presented in a converted art deco film theater near LSU.",
                    Image = "http://farm3.staticflickr.com/2808/9069017950_a3c6ce6f60_b.jpg",
                    SImage = "http://www.rantlifestyle.com/wp-content/uploads/2014/05/rock-ritual-at-varsity-new2.jpg"
                }
                 );
            venues.Add(
                new Venue
                {
                    Name = "Howlin' Wolf",
                    PhysicalAddress = new Address
                    {
                        AddressLine1 = "907 S Peters Street",
                        City = "New Orleans",
                        State = "LA",
                        ZipCode = "70130"
                    },
                    Capacity = 100,
                    Description = "Musical acts shine at this expansive, low-key concert hall with a mahogany bar & pub fare.",
                    Image = "http://s3.evcdn.com/images/edpborder500/I0-001/003/924/214-6.jpeg_/howlin-wolf-14.jpeg",
                    SImage = "http://farm4.static.flickr.com/3368/4636940132_686f854676.jpg"
                }
                );
            venues.Add(
                new Venue
                {
                    Name = "Red Rocks Amphitheatre",
                    PhysicalAddress = new Address
                    {
                        AddressLine1 = "18300 W Alameda Pkwy",
                        City = "Morrison",
                        State = "CO",
                        ZipCode = "80465"
                    },
                    Capacity = 100,
                    Description = "Red Rocks Amphitheatre is a rock structure near Morrison, Colorado, 10 miles west of Denver, where concerts are given in the open-air amphitheatre.",
                    Image = "https://www.jambase.com/wp-content/uploads/photo-gallery/20160507%20Papadosio%20Red%20Rocks%20Phierce%20Photography/Papadosio%20Red%20Rocks%20Phierce%20Photo%20(5).jpg"
                }
                );

            dataContext.SaveChanges();
        }

        private static void SeedUsers(DbContext dataContext)
        {
            var users = dataContext.Set<User>();

            if (users.Any())
            {
                AddAdminUser(dataContext);
                return;
            }
            AddAdminUser(dataContext);
            for (int i = 0; i < 5; i++)
            {

                users.Add(new User
                {
                    Email = $"email{i}@envoc.com",
                    Password = CryptoHelpers.HashPassword($"password{i}"),
                    Role = "Customer",
                    BillingAddress = new Address
                    {
                        AddressLine1 = "123 place St",
                        City = "Hammond",
                        State = "LA",
                        ZipCode = "70403"
                    }
                });
            }
            dataContext.SaveChanges();
        }

        private static void AddAdminUser(DbContext dataContext)
        {
            var users = dataContext.Set<User>();
            if (users.Any(x => x.Email == "admin@envoc.com"))
            {
                return;
            }
            users.Add(new User
            {
                Email = $"admin@envoc.com",
                Password = CryptoHelpers.HashPassword("password"),
                Role = "Admin",
                BillingAddress = new Address
                {
                    AddressLine1 = "123 place St",
                    City = "Hammond",
                    State = "LA",
                    ZipCode = "70403"
                }
            });
            dataContext.SaveChanges();
        }
    }
}