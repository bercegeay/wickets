﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Validators;
using Microsoft.EntityFrameworkCore;
using SP18.PF.Core.Features.Events;
using SP18.PF.Core.Features.Tours;
using SP18.PF.Core.Features.Venues;

namespace SP18.PF.Web.Areas.Admin.Models.Events
{
    public class EventCreateViewModelValidator : AbstractValidator<EventCreateViewModel>
    {
        private readonly DbContext context;

        public EventCreateViewModelValidator(DbContext context, IValidator<EventBaseViewModel> baseValidator)
        {
            this.context = context;
            RuleFor(x => x)
                .CustomAsync(TimeSlot)
                .CustomAsync(TourDuplicacy)
                .SetValidator(baseValidator);
            RuleFor(x => x.TourId)
                .NotNull()
                .CustomAsync(BeValidTourId);
            RuleFor(x => x.VenueId)
                .NotNull()
                .CustomAsync(BeValidVenueId);
        }

        private async Task TourDuplicacy(EventCreateViewModel record, CustomContext e, CancellationToken token)
        {
            var RT = record.TourId;

            var TourisScheduled = await context.Set<Event>()
                .AnyAsync(x => x.TourId == RT
                && (x.EventStart <= record.EventStart
                && x.EventEnd >= record.EventEnd)
                || (x.EventStart >= record.EventStart
                && x.EventStart <= record.EventEnd)
                , token);

            if (TourisScheduled)
            {
                e.AddFailure("Conflicting Schedule: This tour is already performing during this time.");
            }

        }

        private async Task TimeSlot(EventCreateViewModel record, CustomContext e, CancellationToken token)
        {
            TimeSpan duration = new TimeSpan(1, 0, 0);

            var EndValidation = await context.Set<Event>()
                .AnyAsync(x => x.VenueId == record.VenueId
                && x.EventStart >= record.EventStart.GetValueOrDefault().Subtract(duration)
                && x.EventStart <= record.EventEnd.GetValueOrDefault().AddHours(1), token);

            var StartValidation = await context.Set<Event>()
                .AnyAsync(x => x.VenueId == record.VenueId
                && x.EventEnd <= record.EventEnd.GetValueOrDefault().AddHours(1)
                && x.EventEnd >= record.EventStart.GetValueOrDefault().Subtract(duration), token);

            var OverlappingValidation = await context.Set<Event>()
                .AnyAsync(x => x.VenueId == record.VenueId
                && x.EventStart > record.EventStart
                && x.EventStart < record.EventEnd
                , token);

            var BetweenValidation = await context.Set<Event>()
                .AnyAsync(x => x.VenueId == record.VenueId
                && x.EventStart <= record.EventStart
                && x.EventEnd >= record.EventEnd
                , token);

            if (StartValidation)
            {
                e.AddFailure("Conflicting Schedule: A tour must schedule at least one hour ahead of a scheduled performance.");
            }

            if (EndValidation)
            {
                e.AddFailure("Conflicting Schedule: The performance must end at least an hour before another.");
            }

            if (BetweenValidation)
            {
                e.AddFailure("Conflicting Schedule: You are attempting to schedule a performance within a scheduled performance.");
            }

            if (OverlappingValidation)
            {
                e.AddFailure("Conflicting Schedule: You are attempting to schedule a performance that overlaps another.");
            }

        }

        private async Task BeValidTourId(int tourId, CustomContext e, CancellationToken token)
        {
            var matchingTours = await context.Set<Tour>().AnyAsync(x => x.Id == tourId, token);
            if (!matchingTours)
            {
                e.AddFailure("No such tour record.");
            }
        }

        private async Task BeValidVenueId(int venueId, CustomContext e, CancellationToken token)
        {
            var matchingVenues = await context.Set<Venue>().AnyAsync(x => x.Id == venueId, token);
            if (!matchingVenues)
            {
                e.AddFailure("No such venue record.");
            }
        }
    }
}