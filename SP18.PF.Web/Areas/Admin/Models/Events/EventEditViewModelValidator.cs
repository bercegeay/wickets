﻿using FluentValidation;
using FluentValidation.Validators;
using Microsoft.EntityFrameworkCore;
using SP18.PF.Core.Features.Events;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SP18.PF.Web.Areas.Admin.Models.Events
{
    public class EventEditViewModelValidator : AbstractValidator<EventEditViewModel>
    {

        private readonly DbContext context;

        public EventEditViewModelValidator( IValidator<EventBaseViewModel> baseValidator)
        {
            RuleFor(x => x)
                .SetValidator(baseValidator);
        }

        private async Task TimeSlot(EventEditViewModel record, CustomContext e, CancellationToken token)
        {
            TimeSpan duration = new TimeSpan(1, 0, 0);

            try
            {
                var RT = record.VenueId;
                var TourisScheduled = await context.Set<Event>()
                    .AnyAsync(x => x.VenueId == RT
                    && (x.EventStart <= record.EventStart.GetValueOrDefault().Subtract(duration)
                    && x.EventEnd >= record.EventEnd.GetValueOrDefault().AddHours(1))
                    || (x.EventStart >= record.EventStart.GetValueOrDefault().Subtract(duration)
                    && x.EventEnd <= record.EventEnd.GetValueOrDefault().AddHours(1))
                    || (x.EventEnd <= record.EventEnd.GetValueOrDefault().AddHours(1)
                    && x.EventEnd >= record.EventStart.GetValueOrDefault().Subtract(duration))
                    || (x.EventStart >= record.EventStart.GetValueOrDefault().Subtract(duration)
                    && x.EventStart <= record.EventEnd.GetValueOrDefault().AddHours(1))
                    , token);

                if (TourisScheduled)
                {
                    e.AddFailure("Conflicting Schedule: This time slot is unavailable.");
                }
            }
            catch (NullReferenceException) { }
            catch (ArgumentOutOfRangeException) { }
            catch (InvalidOperationException) { };

        }
    }
}