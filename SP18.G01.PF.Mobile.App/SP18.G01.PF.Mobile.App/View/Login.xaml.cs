﻿using SP18.G01.PF.Mobile.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SP18.G01.PF.Mobile.App.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Login : ContentPage
	{
        public Login()
        {
            BindingContext = new LoginDTO()
            {
                Email = "",
                Password = "",
            };
        
			InitializeComponent ();
		}


        private void Button_Clicked_Login(object sender, EventArgs e)
        {
            var login = (LoginDTO)BindingContext;
            if (login.Login())
            {
                Navigation.PushAsync(new MainPage());
            }
            else
            {
                Console.WriteLine("Login Failed");
            }
        }
    }
}