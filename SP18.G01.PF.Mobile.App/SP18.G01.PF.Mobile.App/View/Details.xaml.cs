﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace SP18.G01.PF.Mobile.App.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Details : ContentPage
	{
		public Details ()
		{
			InitializeComponent ();
            BindingContext = AccountData.TicketSelect;
        }

        public void Button_Clicked_Maps(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("http://maps.google.com/maps"));
        }

        public void Button_Clicked_QR(object sender, EventArgs e)
        {
            Navigation.PushAsync(new QR());
        }
    }
}