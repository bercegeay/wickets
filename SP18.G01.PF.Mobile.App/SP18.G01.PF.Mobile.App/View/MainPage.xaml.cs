﻿using SP18.G01.PF.Mobile.App.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SP18.G01.PF.Mobile.App
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        private void Button_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Contact());
        }

        private void Button_Clicked_About(object sender, EventArgs e)
        {
            Navigation.PushAsync(new About());
        }

        private void Button_Clicked_Tickets(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Tickets());
        }
    }
}
