﻿using Newtonsoft.Json;
using SP18.G01.PF.Mobile.App.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Xamarin.Forms;
namespace SP18.G01.PF.Mobile.App.View
{
    public partial class Tickets : ContentPage
    {
        public class Ticket
        {
            public int Id { get; set; }

            public int UserId { get; set; }

            public decimal PurchasePrice { get; set; }

            public string VenueName { get; set; }

            public string TourName { get; set; }

        }
        public Tickets()
        {
            InitializeComponent();
            LoadData();
            ListView1.ItemsSource = AccountData.UserTicket;
        }

        public void LoadData()
        {
            Label header = new Label
            {
                Text = "ListView",
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                HorizontalOptions = LayoutOptions.Center
            };
            var content = "";
            var cookieContainer = new CookieContainer();

            // HttpClient client = new HttpClient();
         
            var RestURL = new Uri(Connection.api + "tickets");

            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = RestURL })
            {
                cookieContainer.Add(RestURL, new Cookie("ARRAffinity", AccountData.Aff));


                cookieContainer.Add(RestURL, new Cookie(".AspNetCore.Cookies", AccountData.Token));

                //client.BaseAddress = new Uri(RestURL);
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(RestURL).Result;

                content = response.Content.ReadAsStringAsync().Result;

                var Items = JsonConvert.DeserializeObject<List<TicketDTO>>(content);

                AccountData.UserTicket = Items;
                
            }
        }

        public void SelectTicket(object stub, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null || e.SelectedItem == "")
            {
                return;
            }
            var UserTicket = e.SelectedItem as Models.TicketDTO;
            AccountData.TicketSelect = UserTicket;
            Navigation.PushAsync(new Details());
            ListView1.SelectedItem = null;

        }
    }

}
