﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SP18.G01.PF.Mobile.App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Contact : ContentPage
	{
		public Contact ()
		{
			InitializeComponent ();
		}

        public void Button_Clicked_Email(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("mailto:Wickets@test.com"));
        }
	}
}