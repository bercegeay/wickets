﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SP18.G01.PF.Mobile.App.Models
{
    public class TicketDTO
    {
        public int Id { get; set; }
        public double Price { get; set; }
        public EventDTO Event { get; set; }
    }
}
