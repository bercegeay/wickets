﻿using Newtonsoft.Json;
using SP18.G01.PF.Mobile.App.View;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Xamarin.Forms;

namespace SP18.G01.PF.Mobile.App.Models
{
    public class TicketData
    {
        public static List<TicketDTO> GetClientTickets()
        {
            var address = new Uri(Connection.api + "tickets");

            var cookieContainer = new CookieContainer();

            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler) { BaseAddress = address })
            {
                cookieContainer.Add(address, new Cookie("ARRAffinity", AccountData.Aff));


                cookieContainer.Add(address, new Cookie(".AspNetCore.Cookies", AccountData.Token));

                var re = client.GetAsync("").Result;

                var jsonStringResult = re.Content.ReadAsStringAsync().Result;

                var ticket = JsonConvert.DeserializeObject<List<TicketDTO>>(jsonStringResult);

                return ticket;
            }
        }
    }
}
