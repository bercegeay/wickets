﻿
namespace SP18.G01.PF.Mobile.App
{
    public class Ticket
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public decimal PurchasePrice { get; set; }

        public string VenueName { get; set; }

        public string TourName { get; set; }

    }
}
