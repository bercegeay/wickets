﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace SP18.G01.PF.Mobile.App.Models
{
    public class LoginDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public bool Login()
        {
            CookieContainer cookieContainer = new CookieContainer();
            HttpClientHandler handler = new System.Net.Http.HttpClientHandler()
            {
                CookieContainer = cookieContainer
            };
            var client = new HttpClient(handler);
            var address = new Uri(Connection.api + "users/login");
            var content = new StringContent(JsonConvert.SerializeObject(this), Encoding.UTF8, "application/json");
            HttpResponseMessage result = client.PostAsync(address, content).Result;

            var jsonResult = result.Content.ReadAsStringAsync().Result;

            try
            {
                AccountData.User = JsonConvert.DeserializeObject<LoginResult>(jsonResult);

                Hashtable table = (Hashtable)cookieContainer.GetType().InvokeMember("m_domainTable", System.Reflection.BindingFlags.NonPublic |
                    System.Reflection.BindingFlags.GetField |

                    System.Reflection.BindingFlags.Instance,
                    null,

                    cookieContainer,
                    new object[] { });



                var cookies = new Dictionary<string, string>();

                var theFirst = true;

                foreach (var key in table.Keys)
                {
                    if (theFirst)
                        if (cookieContainer.GetCookies(new Uri(string.Format("https://{0}/", key))).Count > 0)
                        {
                            foreach (Cookie cookie in cookieContainer.GetCookies(new Uri(string.Format("https://{0}/", key))))
                            {
                                cookies.Add(cookie.Name, cookie.Value);
                            }
                        }
                    theFirst = false;
                }


                AccountData.Token = cookies.First(r => r.Key.Equals(".AspNetCore.Cookies")).Value;

                AccountData.Aff = cookies.First(r => r.Key.Equals("ARRAffinity")).Value;

                AccountData.Cookie = "AARAfinity=" + AccountData.Aff + "; AspNetCore.Cookies=" + AccountData.Token;
                return true;
            }


            catch (Exception)
            {
                AccountData.Aff = null;
                AccountData.Cookie = null;
                AccountData.User = null;
                AccountData.Token = null;
                return false;
            }




        }
    }
}

public class LoginResult
{
    public int Id { get; set; }
    public string Email { get; set; }
    public string Role { get; set; }
}


