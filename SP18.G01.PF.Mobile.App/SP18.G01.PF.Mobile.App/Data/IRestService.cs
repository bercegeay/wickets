﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SP18.G01.PF.Mobile.App.Data
{
    public interface IRestService
    {
        Task<List<Ticket>> RefreshDataAsync();

        Task SaveTicket(Ticket item, bool isNewItem);
    }
}
