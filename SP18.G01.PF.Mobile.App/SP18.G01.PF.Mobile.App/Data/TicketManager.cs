﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SP18.G01.PF.Mobile.App.Data
{
    public class TicketManager
    {
        IRestService restService;

        public TicketManager(IRestService service)
        {
            restService = service;
        }

        public Task<List<Ticket>> GetTasksAsync()
        {
            return restService.RefreshDataAsync();
        }

        public Task SaveTaskAsync(Ticket item, bool isNewItem = false)
        {
            return restService.SaveTicket(item, isNewItem);
        }

    }
}
