﻿using SP18.G01.PF.Mobile.App.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SP18.G01.PF.Mobile.App
{
    public static class Connection
    {
        public static string api = "https://sp18-pf-g01.azurewebsites.net/api/";
    }

    public static class AccountData
    {
        public static string Aff { get; set; }
        public static string Token { get; set; }
        public static string Cookie { get; set; }
        public static LoginResult User { get; set; }

        public static TicketDTO TicketSelect { get; set; }
        public static List<TicketDTO> UserTicket {get;set;}

    }
}
////////////////////////////////////////////////////////
//Riders on the storm
//Riders on the storm
//Into this house we're born
//Into this world we're thrown
//Like a dog without a bone
//An actor out on loan
//Riders on the storm


//The Doors
////////////////////////////////////////////////////////